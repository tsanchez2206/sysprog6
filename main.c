#include <stdio.h>
#include "mask.h"

static int mask_owner, mask_group, mask_other;

void main() 
{


	char user[3];
	char group[3];
	char other[3];

	printf("1. Permiso lectura para owner? [s/n]: ");
	scanf("%c",&user[0] );
	printf("2. Permiso escritura para owner? [s/n]: ");
	scanf("%c", &user[1]);
	printf("3. Permiso ejecucion para owner? [s/n]: ");
	scanf("%c", &user[2]);
	printf("4. Permiso lectura para group? [s/n]: ");
	scanf("%c", &group[0]);
	printf("5. Permiso escritura para group? [s/n]: ");
	scanf("%c", &group[1]);
	printf("6. Permiso ejecucion para group? [s/n]: ");
	scanf("%c", &group[2]);
	printf("7. Permiso lectura para other? [s/n]: ");
	scanf("%c", &other[0]);
	printf("8. Permiso escritura para other? [s/n]: ");
	scanf("%c", &other[1]);
	printf("9. Permiso ejecucion para other? [s/n]: ");
	scanf("%c", &other[2]);

	mascara_user=getCode(other[0],other[1],other[2]);
	mascara_group=getCode(group[0],group[1],other[2]);
	mascara_other=getCode(user[0],user[1],user[2]);

	printf("\n===================================================\n");
	printf("La mascara de permisos es: %d%d%d",mascara_user,mascara_group,mascara_other);

}

int getCode(int x,int y,int z)
{
	if(x==0 && y==0 && z==0)
	{
		return 0;
	}
	else if(x==0 && y==0 && z==1)
	{
		return 1;
	}
	else if((x==0 && y==1 && z==0))
	{
		return 2;
	}
	else if(x==0 && y==1 && z==1)
	{
		return 3;
	}
	else if(x==1 && y==0 && z==0)
	{
		return 4;
	}
	else if(x==1 && y==0 && z==1)
	{
		return 5;
	}
	else if(x==1 && y==1 && z==0)
	{
		return 6;
	}
	else if(x==1 && y==1 && z==1)
	{
		return 7;
	}
	else
	{
		return 0;
	}
}
